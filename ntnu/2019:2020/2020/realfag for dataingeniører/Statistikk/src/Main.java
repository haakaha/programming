import java.util.ArrayList;
import java.util.Collections;

public class Main {
    private static final int AMOUNT = 100;
    private static final int MARKED_AMOUNT = 15;

    private static final int SELECTION = 10;
    private static final int REPETITIONS = 25;

    public static void main(String[] args) {
        int[] results = new int[REPETITIONS];

        for (int i = 0; i < REPETITIONS; i++) {
            ArrayList<Boolean> data = new ArrayList<>();

            //Initializes test data
            for (int j = 0; j < AMOUNT; j++) {
                if (j < MARKED_AMOUNT) data.add(true);
                else data.add(false);
            }

            //Shuffles the test data
            Collections.shuffle(data);

            int result = 0;

            //Checks the first values
            for (int j = 0; j < SELECTION; j++) {
                if(data.get(data.size() - 1)) result++;
                data.remove(data.size() - 1);
            }

            //Stores the result
            results[i] = result;
        }

        //Calculates the average
        int total = 0;
        for(int i = 0; i < results.length; i++) {
            total += results[i];
            System.out.println((i+1) + ": " + results[i]);
        }
        double average = (double) total / (double) REPETITIONS;

        System.out.println(average);
    }
}