//@flow
import express from 'express';

const util = require('util');
const exec = util.promisify(require('child_process').exec);

import path from 'path';
import reload from 'reload';
import fs from 'fs';

const public_path = path.join(__dirname, '/../../client/public');

const app = express();

app.use(express.static(public_path));
app.use(express.json());

app.post('/compile', (req, res) => {
   const code = req.body.code;

   //Writes C++ file to file system
   fs.writeFileSync('./main.cpp', code, function(error) {
      if (error) return console.log(error);
   });

   //Compiles thecode
   exec('g++ -Wall main.cpp', (error, stdout, stderr) => {
      let result = '';

      if(error){
         result += error; 
         res.json(result); 
         return;
      }

      if (stderr) {
         result += stderr;
         result += '\n'; 
      }

      exec('./a.out', (error, stdout, stderr) => {
         if (stderr) {
            res.json(stderr);
            console.error(stderr);

         } else {
            result += 'Output:\n';
            result += stdout;
            res.json(result);
            return; 
         }
      });
   });
});

reload(app).then(reloader => {
   app.listen(4000, (error: ?Error) => {
      if (error) console.error(error);
      console.log('Express server started');
      reloader.reload();
   });
});
