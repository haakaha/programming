// @flow
import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';

import { compiler } from './services';

import AceEditor from 'react-ace';

import 'ace-builds/src-noconflict/mode-javascript';
import 'ace-builds/src-noconflict/theme-monokai';

import './main.css';

class App extends Component {
    code: string = '';
    output: string = '';

    compileCode = () => {
        compiler
            .compile(this.code)
            .then(res => {
               this.output = res.data;
            })
            .catch(error => console.error(error));
    };

    change = newValue => {
        this.code = newValue;
    };

    render() {
        return (
            <div className='container'>
                <button className='compile__button' onClick={this.compileCode}>
                    Run Code
                </button>

                <AceEditor mode='javascript' value={this.code} width='100vw' height='70vh' theme='monokai' onChange={this.change} className='input' editorProps={{ $blockScrolling: true }} />

                <p className='output'>
                    {this.output.split('\n').map((paragraph, i) => (
                        <span key={i}>
                            {paragraph}
                            <br />
                        </span>
                    ))}
                </p>
            </div>
        );
    }
}

const root = document.querySelector('.root');
if (root) ReactDOM.render(<App />, root);

export default App;
