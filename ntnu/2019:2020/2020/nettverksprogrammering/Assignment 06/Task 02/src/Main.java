import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Main {
    private static final String PERSISTENCE_UNIT_NAME = "accounts";
    private static EntityManagerFactory factory;

    public static void main(String[] args) {

        //Creates entity manager
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();

        //Initializes database
        Database database = new Database(em);
        database.init();

        int val = 100;

        //Gets account with balance over a certain amount
        System.out.println("\n-- ACCOUNTS WITH OVER " + val + " IN BALANCE --");
        List<Account> accounts = database.getAccounts(val);
        for (Account account : accounts) System.out.println(account);

        //Changes the owner of the first object
        Account updatedAccount = accounts.get(0);
        updatedAccount.setOwner("Ola Nordmann");

        //Saves the change to the database
        database.update(updatedAccount);

        System.out.println("\n(UPDATING OWNER OF FIRST ACCOUNT)");

        //Prints the updated accounts
        System.out.println("\n-- ACCOUNTS WITH OVER " + val + " IN BALANCE --");
        for (Account account : accounts){
            database.refresh(account);
            System.out.println(account);
        }

        //Closes EntityManager
        database.close();
    }
}
