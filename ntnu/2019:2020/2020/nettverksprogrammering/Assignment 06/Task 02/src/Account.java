import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double balance;
    private String owner;

    public Account(){

    }

    public Account(double balance, String owner){
        this.balance = balance;
        this.owner = owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String ownder) {
        this.owner = ownder;
    }

    public void withdraw(double amount){
        this.balance -= Math.abs(amount);
    }

    @Override
    public String toString() {
        return "Account [balance = " + balance + ", owner = " + owner + "]";
    }
}