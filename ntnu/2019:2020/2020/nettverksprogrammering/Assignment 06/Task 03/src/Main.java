import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Example of a program where the result is incorrect
 *
 * Expected: Jens: 0,   Kari: 700
 * Reality:  Jens: 100, Kari 600
 *
 * One transaction is lost
 */
public class Main {
    private static final String PERSISTENCE_UNIT_NAME = "accounts";
    private static EntityManagerFactory factory;

    public static void main(String[] args) {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

        //Initializes database
        Database database = new Database(factory.createEntityManager());
        database.init();

        //Prints data before transactions
        System.out.println("-- BEFORE TRANSACTIONS --");
        List<Account> accounts = database.getAccounts();
        for(Account account : accounts) System.out.println(account);
        System.out.println();

        //Creates threads
        Thread[] threads = {
                new TransactionThread(new Database(factory.createEntityManager()), 500),
                new TransactionThread(new Database(factory.createEntityManager()), 1000)
        };

        //Starts threads
        for(Thread thread : threads)
            thread.start();

        //Waits for threads to finish
        try {
            for(Thread thread : threads)
                thread.join();
        } catch (Exception e){
            e.printStackTrace();
        }

        //Prints data after transactions
        System.out.println("\n-- AFTER TRANSACTIONS --");
        for(Account account : accounts){
            database.refresh(account);
            System.out.println(account);
        }

        //Closes connection
        database.close();
    }
}