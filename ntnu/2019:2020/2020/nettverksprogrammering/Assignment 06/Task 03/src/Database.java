import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class Database {
    private EntityManager em;

    public Database(EntityManager em){
        this.em = em;
    }

    //Initializes the database
    public void init(){

        //Deletes old data
        try {
            em.getTransaction().begin();
            em.createQuery("DELETE FROM Account account").executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e){
            System.out.println("Nothing to delete from database");
        }

        //Creates new accounts
        Account[] newAccounts = {
                new Account(200, "Jens Jensen"),
                new Account(500, "Kari Nordmann"),
                new Account(1000, "Grete Olsen")
        };

        //Uploads new accounts to database
        for(Account newAccount : newAccounts){
            em.getTransaction().begin();
            em.persist(newAccount);
            em.getTransaction().commit();
        }
    }

    public List<Account> getAccounts(){
        Query query = em.createQuery("SELECT account FROM Account account");
        return query.getResultList();
    }

    public void refresh(Account account){
        em.refresh(account);
    }

    public void update(Account account){
        em.getTransaction().begin();
        em.merge(account);
        em.getTransaction().commit();
    }

    public void close(){
        em.close();
    }
}