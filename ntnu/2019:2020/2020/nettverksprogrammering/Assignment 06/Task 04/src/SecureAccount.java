import javax.persistence.*;

@Entity
public class SecureAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    public int lockField;

    private double balance;
    private String owner;

    public SecureAccount(){

    }

    public SecureAccount(double balance, String owner){
        this.balance = balance;
        this.owner = owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String ownder) {
        this.owner = ownder;
    }

    public void withdraw(double amount){
        this.balance -= amount;
    }

    public void deposit(double amount){
        this.balance += amount;
    }

    @Override
    public String toString() {
        return "Account [balance = " + balance + ", owner = " + owner + "]";
    }
}