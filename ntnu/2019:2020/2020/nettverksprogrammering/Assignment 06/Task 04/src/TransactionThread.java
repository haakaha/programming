import java.util.List;

public class TransactionThread extends Thread {
    private Database database;
    private int delay;

    public TransactionThread(Database database, int delay){
        this.database = database;
        this.delay = delay;
    }

    public void run() {
        System.out.println("TRANSFERRING 100 FROM JENS TO KARI...");

        //Retrieves accounts from the database
        List<SecureAccount> secureAccounts = database.getAccounts();

        SecureAccount sender = secureAccounts.get(0);
        SecureAccount receiver = secureAccounts.get(1);

        //Thread sleeps
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e){
            e.printStackTrace();
        }

        //Does a transaction
        sender.withdraw(100);
        receiver.deposit(100);

        //Updates values in the database
        database.update(sender);
        database.update(receiver);

        //Closes connection
        database.close();
    }
}
