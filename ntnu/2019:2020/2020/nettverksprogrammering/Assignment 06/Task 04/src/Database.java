import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import java.util.List;

public class Database {
    private EntityManager em;

    public Database(EntityManager em){
        this.em = em;
    }

    //Initializes the database
    public void init(){

        //Deletes old data
        try {
            em.getTransaction().begin();
            em.createQuery("DELETE FROM SecureAccount account").executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e){
            System.out.println("Nothing to delete from database");
        }

        //Creates new accounts
        SecureAccount[] newAccounts = {
                new SecureAccount(200, "Jens Jensen"),
                new SecureAccount(500, "Kari Nordmann"),
                new SecureAccount(1000, "Grete Olsen")
        };

        //Uploads new accounts to database
        for(SecureAccount newAccount : newAccounts){
            em.getTransaction().begin();
            em.persist(newAccount);
            em.getTransaction().commit();
        }
    }

    public List<SecureAccount> getAccounts(){
        Query query = em.createQuery("SELECT account FROM SecureAccount account");
        return query.getResultList();
    }

    public void refresh(SecureAccount account){
        em.refresh(account);
    }

    public void update(SecureAccount account){
        em.getTransaction().begin();
        em.merge(account);
        em.getTransaction().commit();
    }

    public void close(){
        em.close();
    }
}