import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
    public static void main(String[] args) {
        final int PORTNR = 80;

        try (
            //Initializes connection
            ServerSocket serverSocket = new ServerSocket(PORTNR);
            Socket connection = serverSocket.accept();

            //Initializes reader
            InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());
            BufferedReader reader = new BufferedReader(streamReader);

            //Initializes writer
            PrintWriter writer = new PrintWriter(connection.getOutputStream(), true);
        ){
            System.out.println("Connection established");

            //Reads the header from browser
            String HTTPHeader = "";
            String headerLine = reader.readLine();
            while (!headerLine.equals("")) {
                HTTPHeader += "<LI>" + headerLine + "</LI>";
                headerLine = reader.readLine();
            }
            
            //Creates header response
            String[] HTTPResponse = {
                "HTTP/1.0 200 OK",
                "Content-Type: text/html; charset=utf-8",
                "",
                "<HTML><BODY>",
                "<H1> Hilsen. Du har koblet deg opp til min enkle web-tjener </h1>",
                "<UL>",
                HTTPHeader,
                "</UL>",
                "</BODY></HTML>"
            };

            //Returns header
            for(String line : HTTPResponse)
                writer.println(line);

            System.out.println("Sent HTTP-response");
            System.out.println("Connection terminated");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}