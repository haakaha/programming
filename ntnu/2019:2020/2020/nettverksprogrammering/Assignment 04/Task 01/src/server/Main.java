package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
    public static void main(String[] args){
        final int PORTNR = 1250;

        try (
            //Initializes connection
            ServerSocket serverSocket = new ServerSocket(PORTNR);
        ){
            while(true){
                System.out.println("Waiting for client... ");
                Socket connection = serverSocket.accept();
                System.out.println("Client connected!");

                Thread clientThread = new ClientThreadHandler(connection);
                clientThread.start();
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}