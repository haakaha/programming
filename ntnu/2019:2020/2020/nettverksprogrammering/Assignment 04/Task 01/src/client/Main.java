package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        final int PORTNR = 1250;
        final String IP_ADDRESS = "localhost";

        //Used for user input
        Scanner scanner = new Scanner(System.in);

        try (
            //Initializes connection
            Socket connection = new Socket(IP_ADDRESS, PORTNR);

            //Initializes reader
            InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());
            BufferedReader reader = new BufferedReader(streamReader);

            //Initializes writer
            PrintWriter writer = new PrintWriter(connection.getOutputStream(), true);
        ){
            System.out.println("Connected to server on " + IP_ADDRESS);
            System.out.println("\n" + reader.readLine() + "\n" + reader.readLine() + "\n");

            //Reads user input
            String line = scanner.nextLine();
            while(!line.equalsIgnoreCase("END")){
                writer.println(line);
                System.out.println(reader.readLine() + "\n");
                line = scanner.nextLine();
            }
        } catch(IOException e){
            e.printStackTrace();
            System.out.println("Connection unsuccessful");
        }
    }
}