cmake_minimum_required(VERSION 3.15)
project(Assignment_02)

set(CMAKE_CXX_STANDARD 14)
SET(CMAKE_CXX_FLAGS -pthread)

add_executable(Assignment_02 main.cpp)