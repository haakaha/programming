package server;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.Map;

class ReadWriteHandler implements CompletionHandler<Integer, Map<String, Object>> {
    private AsynchronousSocketChannel clientChannel;

    public ReadWriteHandler(AsynchronousSocketChannel clientChannel) {
        this.clientChannel = clientChannel;
        System.out.println("\nCLIENT CONNECTED!\n");
    }

    @Override
    public void completed(Integer result, Map<String, Object> attachment) {
        Map<String, Object> actionInfo = attachment;
        String action = (String) actionInfo.get("action");

        System.out.println("ACTION: " + action.toUpperCase());

        //Checks if client has disconnected
        if (result == -1) {
            System.out.println("\nCLIENT DISCONNECTED\n");
            return;
        }

        //Writes to client
        if ("read".equals(action)) {
            ByteBuffer buffer = (ByteBuffer) actionInfo.get("buffer");
            buffer.flip();

            //Writes to client
            actionInfo.put("action", "write");
            clientChannel.write(buffer, actionInfo, this);

            buffer.clear();

            System.out.println("REGISTERED NEW CALLBACK/LISTENER FOR CLIENTCHANNEL.READ()\n".toUpperCase());

        //Reads from client
        } else if ("write".equals(action)) {
            ByteBuffer buffer = ByteBuffer.allocate(32);

            actionInfo.put("action", "read");
            actionInfo.put("buffer", buffer);

            clientChannel.read(buffer, actionInfo, this);
            System.out.println("\nREGISTERED NEW CALLBACK/LISTENER FOR CLIENTCHANNEL.WRITE()".toUpperCase());
        }
    }

    @Override
    public void failed(Throwable exc, Map<String, Object> attachment) {
        System.out.println("ERROR!");
    }
}