package server;



public class Main {
    private final static String ADDRESS = "localhost";
    private final static int PORT = 1250;

    public static void main(String[] args) {
        AsyncSocketServer server = new AsyncSocketServer(ADDRESS, PORT);
        server.init();
    }
}