package client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.Future;

public class AsyncSocketClient {
    private AsynchronousSocketChannel client;

    private String address;
    private int port;

    public AsyncSocketClient(String address, int port){
        this.address = address;
        this.port = port;
    }

    //Initializes connection
    public void init() {
        try {
            System.out.print("CONNECTING TO SERVER... ");
            client = AsynchronousSocketChannel.open();
            InetSocketAddress address = new InetSocketAddress(this.address, this.port);
            Future<Void> future = client.connect(address);

            //Waits for connection
            future.get();

            System.out.println("CONNECTED!\n");
        } catch (Exception e) {
            System.out.println("FAILED!");
            throw new IllegalArgumentException("Invalid address/port!");
        }
    }

    //Sends message to server
    public String send(String message) {
        ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());
        Future<Integer> writeResult = client.write(buffer);

        try {
            //Waits for result
            writeResult.get();
        } catch (Exception e){
            e.printStackTrace();
        }

        buffer.flip();

        Future<Integer> readResult = client.read(buffer);

        try {
            //Waits for result
            readResult.get();
        } catch (Exception e){
            e.printStackTrace();
        }

        //Reads result from buffer
        String response = new String(buffer.array()).trim();
        buffer.clear();

        //Returns response
        return response;
    }

    //Closes connections
    public void cleanUp() {
        try {
            client.shutdownInput();
            client.shutdownOutput();
            client.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
