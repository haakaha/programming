package client;

import java.util.Scanner;

public class Main {
    private final static String ADDRESS = "localhost";
    private final static int PORT = 1250;

    public static void main(String[] args) {
        AsyncSocketClient client = new AsyncSocketClient(ADDRESS, PORT);
        client.init();

        Scanner scanner = new Scanner(System.in);

        //Reads user input
        String input = scanner.nextLine();
        while(!input.equalsIgnoreCase("END")) {
            String output = client.send(input);
            System.out.println("SERVER: " + output + "\n");
            input = scanner.nextLine();
        }

        client.cleanUp();
    }
}