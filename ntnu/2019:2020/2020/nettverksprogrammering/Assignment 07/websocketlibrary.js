'use strict';

const net = require('net');

const sha1   = require("crypto-js/sha1");
const Base64 = require("crypto-js/enc-base64");

module.exports = class WebsocketLibrary {
    connections = [];

    handshake = (connection, data) => {
    
        //Adds the new connection to list 
        this.connections.push(connection);

        //Finds the client key in the client handshake 
        let index = data.toString().indexOf('Sec-WebSocket-Key: ') + 'Sec-WebSocket-Key: '.length; 
        let clientKey = data.toString().substring(index, index + 24); 

        //Creates the server key 
        let hash = sha1(clientKey + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11");
        let serverKey = Base64.stringify(hash);

        //Returns server handshake to client 
        connection.write(
              "HTTP/1.1 101 Switching Protocols\r\n"
            + "Upgrade: websocket\r\n" 
            + "Connection: Upgrade\r\n" 
            + "Sec-WebSocket-Accept: " + serverKey + " \r\n\r\n"
            );
    }

    //Transmits data to all clients 
    transmit = data => {
        let message = this.decode(data); 
        let frame = this.frame(message); 
        
        this.connections.forEach(element => {
           element.write(frame); 
        });
    }

    //Decodes the data 
    decode = data => {
        let bytes = Buffer.from(data); 

        //Gets the length of the message
        let length = bytes[1] & 127;

        //Unmasks bytes to decode data 
        let message = ""; 
        for(let i = 6; i < length + 6; i++){
            let j = ((i-2) % 4) + 2; 
            message += String.fromCharCode(bytes[i] ^ bytes[j]); 
        }

        return message; 
    }

    //Frames the message
    frame = message => {

        //Returns error message if message is too long 
        if(message.length > 125)
            return frame("MAX DATA LENGTH OF 126")

        let frame = []; 

        //First bit is type, 81 for text 
        frame.push(0x81); 

        //Second bit is the length 
        frame.push(0b00000000 | message.length); 

        //Remaining bits is the message
        for(const char of Buffer.from(message))
            frame.push(char); 
        
        return Buffer.from(frame); 
    }

    //Removes connection from array 
    close = connection => {
        this.connections = this.connections.filter(element => connection != element)
    }
}