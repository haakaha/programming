'use strict';

const WebsocketLibrary = require('./websocketlibrary.js');
const net = require('net');

//Simple HTTP server responds with a simple WebSocket client test
const HTTPServer = net.createServer(connection => {
    connection.on('data', () => {
        let content = `<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
  </head>
  <body>
    WebSocket test page
    <script>
      let ws = new WebSocket('ws://localhost:3001');
      ws.onmessage = event => alert('Message from server: ' + event.data);
      ws.onopen = () => ws.send('hello');
    </script>
  </body>
</html>
`;
        connection.write('HTTP/1.1 200 OK\r\nContent-Length: ' + content.length + '\r\n\r\n' + content);
    });
});
HTTPServer.listen(3000, () => {
    console.log('HTTP server listening on port 3000');
});

//WebSocket server
const WSServer = net.createServer(connection => {
    console.log('Client connected');
    
    const websocket = new WebsocketLibrary(); 

    connection.on('data', data => {
        if(data.toString().includes("Connection: Upgrade"))
          websocket.handshake(connection, data); 
        else 
          websocket.transmit(data); 
    });

    connection.on('end', () => {
        console.log('Client disconnected');
        websocket.close(connection); 
        connection.end(); 
    });
});

WSServer.on('error', error => {
    console.error('Error: ', error);
});

WSServer.listen(3001, () => {
    console.log('WebSocket server listening on port 3001');
});
