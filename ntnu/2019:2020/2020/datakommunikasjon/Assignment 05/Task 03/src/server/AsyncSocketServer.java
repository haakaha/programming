package server;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.HashMap;
import java.util.Map;

public class AsyncSocketServer {
    private AsynchronousServerSocketChannel serverChannel;

    private String address;
    private int port;

    public AsyncSocketServer(String address, int port){
        this.address = address;
        this.port = port;
    }

    public void init(){
        System.out.println("STARTING SERVER... ");

        try {
            serverChannel = AsynchronousServerSocketChannel.open();
            serverChannel.bind(new InetSocketAddress(this.address, this.port));

            System.out.println("MAKING CLIENT HANDLER");

            serverChannel.accept(
                    null, new CompletionHandler<AsynchronousSocketChannel, Object>() {
                        @Override
                        public void completed(AsynchronousSocketChannel result, Object attachment){

                            //Accepts new clients
                            if(serverChannel.isOpen()) serverChannel.accept(null, this);

                            //New client
                            AsynchronousSocketChannel clientChannel = result;
                            if(clientChannel == null || !clientChannel.isOpen()) return;

                            //Handles communication with client
                            ReadWriteHandler handler = new ReadWriteHandler(clientChannel);//
                            ByteBuffer buffer = ByteBuffer.allocate(32);

                            //Used for reading
                            Map<String, Object> readInfo = new HashMap<>();
                            readInfo.put("action", "read");
                            readInfo.put("buffer", buffer);

                            //Reads from client
                            clientChannel.read(buffer, readInfo, handler);
                        }

                        @Override
                        public void failed(Throwable exc, Object attachment) {
                            System.out.println("ERROR!");
                        }
                    }
            );

            //Keeps server running
            System.in.read();
        } catch (Exception e){
            System.out.println("FAILED!");
        }
    }
}