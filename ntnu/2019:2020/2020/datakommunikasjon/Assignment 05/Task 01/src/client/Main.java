package client;

import java.net.*;
import java.util.Scanner;

public class Main {
    private final static String ADDRESS = "localhost";
    private final static int PORT = 1250;

    public static void main(String[] args) {

        //Used for reading user input
        Scanner scanner = new Scanner(System.in);

        //Initializes the connection
        try (DatagramSocket socket = new DatagramSocket()){
            System.out.println("EXIT PROGRAM: WRITE 'END'");
            System.out.println("FORMAT: ADD/SUB NUM1 NUM2\n");

            String input = scanner.nextLine();
            while(!input.equalsIgnoreCase("END")){

                //Sends input to server
                byte[] buffer = input.getBytes();
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(ADDRESS), PORT);
                socket.send(packet);

                //Receives output from server
                buffer = new byte[256];
                packet = new DatagramPacket(buffer, buffer.length);
                socket.receive(packet);

                //Prints output
                String output = new String(packet.getData(), 0, packet.getLength());
                System.out.println(output + "\n");

                //Reads next line
                input = scanner.nextLine();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}