package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ServerThread extends Thread {
    private DatagramSocket socket;

    public ServerThread(DatagramSocket socket) {
        this.socket = socket;
    }

    public void run() {
        while (true) {
            try {
                byte[] buffer = new byte[256];
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

                //Receives input from client
                socket.receive(packet);
                String input = new String(packet.getData(), 0, packet.getLength());
                System.out.println("\nRECEIVED " + input.toUpperCase());

                //Processes input from client
                String[] data = input.split("\\s+");
                double[] numbers = convertToNumbers(data);
                String operator = data.length > 0 ? data[0] : "";

                String output;

                //Invalid number of arguments
                if (data.length != 3)
                    output = "INVALID NUMBER OF ARGUMENTS";

                //Invalid operator
                else if (!operator.equalsIgnoreCase("ADD") && !operator.equalsIgnoreCase("SUB"))
                    output = "INVALID OPERATOR";

                //Invalid numbers
                else if (numbers == null)
                    output = "INVALID NUMBERS";

                //No errors
                else
                    output = "ANS " + (operator.equalsIgnoreCase("ADD") ? numbers[0] + numbers[1] : numbers[0] - numbers[1]);

                //Sends output to client
                buffer = output.getBytes();
                System.out.println("SENDING  " + output);
                packet = new DatagramPacket(buffer, buffer.length, packet.getAddress(), packet.getPort());
                socket.send(packet);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //Converts string to double
    private static double[] convertToNumbers(String[] input) {
        try {
            return new double[] {
                    Double.parseDouble(input[1]),
                    Double.parseDouble(input[2])
            };
        } catch (Exception e) {
            return null;
        }
    }
}
