package server;

import java.io.IOException;
import java.net.DatagramSocket;

public class Main {
    private final static int PORT = 1250;

    public static void main(String[] args){
        try {
            DatagramSocket socket = new DatagramSocket(PORT);
            ServerThread serverThread = new ServerThread(socket);
            serverThread.start();

            System.out.println("SERVER STARTED");
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}