#include <condition_variable>
#include <functional>
#include <iostream>
#include <vector>
#include <thread>
#include <atomic>
#include <mutex>
#include <list>

using namespace std;

class Workers
{
public:
    list<function<void()>> tasks;
    condition_variable cv;
    mutex tasks_mutex;

    int thread_count;
    atomic<bool> stopCalled{};

    explicit Workers(int thread_count)
    {
        this -> thread_count = thread_count;
        stopCalled = false;
    }

    //Add tasks to task list
    void post(function<void()> task)
    {
        lock_guard<mutex> lock(tasks_mutex);
        tasks.emplace_back(task);
        cv.notify_one();
    }

    //Executes task after given amount of milliseconds
    void post_timeout(function<void()> task, int timeout)
    {
        this_thread::sleep_for(chrono::milliseconds (timeout));
        task();
    }

    //Starts workers who execute jobs provided
    void start()
    {
        vector<thread> worker_threads;

        for(int i = 0; i < thread_count; i++)
        {
            worker_threads.emplace_back([this]
            {
                while(true)
                {
                    function<void()> task;
                    {
                        unique_lock<mutex> lock(tasks_mutex);

                        //Checks if worker should stop executing tasks
                        if(stopCalled && tasks.empty()) break;

                        //Waits until task list contains at least one task
                        while(tasks.empty())
                            cv.wait(lock);

                        //Gets task to be run
                        task = *tasks.begin();
                        tasks.pop_front();
                    }

                        //Executes task after lock is released
                        task();
                    }
            });
        }

        for (auto &thread : worker_threads)
            thread.join();
    }

    void stop()
    {
        stopCalled = true;
    }
};

int main()
{
    list<thread> threads;

    //Initializes worker threads and event loop
    Workers worker_threads(4);
    Workers event_loop(1);

    //Thread for running worker tasks
    threads.emplace_back([&worker_threads]
    {
        worker_threads.start();
    });

    //Thread for running event loop tasks
    threads.emplace_back([&event_loop]
    {
        event_loop.start();
    });

    //Posts worker threads tasks
    threads.emplace_back([&worker_threads]
    {
        worker_threads.post([] {
            cout << "TASK A" << endl;
        });

        worker_threads.post([] {
            cout << "TASK B" << endl;
        });

        worker_threads.post_timeout([] {
            cout << "TASK E - TIMEOUT" << endl;
        }, 2000);
    });

    //Posts event loop tasks
    threads.emplace_back([&event_loop]
    {
        event_loop.post([] {
            cout << "TASK C" << endl;
        });

        event_loop.post([] {
            cout << "TASK D" << endl;
        });

        event_loop.post_timeout([] {
            cout << "TASK F - TIMEOUT" << endl;
        }, 1000);
    });

    //Stops worker threads and event loop
    worker_threads.stop();
    event_loop.stop();

    for(auto &thread : threads)
        thread.join();
}