#include <iostream>
#include <algorithm>
#include <thread>
#include <vector>
#include <iomanip>

using namespace std;

//Function to check if a given number is prime
bool is_prime(const int &num){
    if(num < 2) return false;

    for(int i = 2; i <= num / 2; i++)
        if (num % i == 0) return false;

    return true;
}

const int THREAD_AMOUNT = 101;
const vector<int> RANGE = { 1, 100 };

int main() {
    const int SIZE = RANGE.back() - RANGE.front();

    vector<thread> threads;
    vector<int> primes;

    mutex prime_mutex;

    primes.reserve(SIZE);
    int interval = SIZE / THREAD_AMOUNT;

    for (int i = 0; i < THREAD_AMOUNT; i++) {
        threads.emplace_back([i, &interval, &primes, &prime_mutex] {

            /*
             * Calculates start and stop-indexes for each thread
             * E.g. Thread 0: 0   - 49
             *      Thread 1: 49  - 99
             *      Thread 2: 100 - 150
             * */
            int startIndex = RANGE.front() + (interval * i);
            int stopIndex = (i == THREAD_AMOUNT - 1) ? (int) RANGE.back() + 1 : startIndex + interval;

            //Loops through each number and checks if prime
            for (int j = startIndex; j < stopIndex; j++) {
                lock_guard<mutex> lock(prime_mutex);
                if (is_prime(j)) primes.push_back(j);
            }

            //Sorts the numbers
            sort(primes.begin(), primes.end());
        });
    }

    //Waits for all threads to complete
    for (auto &thread : threads)
        thread.join();

    //Prints the result
    for(int i = 0; i < primes.size(); i++) {
        if(i % 35 == 0) cout << endl;
        cout << setw(5) << primes[i];
    } cout << endl;
}