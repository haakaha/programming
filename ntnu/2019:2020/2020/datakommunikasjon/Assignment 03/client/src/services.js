// @flow
import axios from 'axios';

class Compiler {
   compile(code: string) {
      //$FlowFixMe
      return axios({
         method: 'post',
         url: '/compile',
         headers: {},
         data: {
            code: code 
         }
      });
   }
}

export let compiler = new Compiler();