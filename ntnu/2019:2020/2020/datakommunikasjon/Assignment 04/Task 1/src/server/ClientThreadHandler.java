package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientThreadHandler extends Thread{
    private Socket connection;

    public ClientThreadHandler(Socket connection){
        this.connection = connection;
    }

    public void run(){
        try (
        //Initializes reader
        InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());
        BufferedReader reader = new BufferedReader(streamReader);

        //Initializes writer
        PrintWriter writer = new PrintWriter(connection.getOutputStream(), true);
        ){
            writer.println("FORMAT: ADD/SUB NUM1 NUM2");
            writer.println("EXIT PROGRAM: WRITE 'END'");

            //Reads input from client
            String line = reader.readLine();
            while(line != null) {
                String[] input = line.split("\\s+");
                double[] numbers = convertToNumbers(input);
                String operator = input.length > 0 ? input[0] : "";

                //Invalid number of arguments
                if(input.length != 3)
                    writer.println("INVALID NUMBER OF ARGUMENTS");

                //Invalid operator
                else if(!operator.equalsIgnoreCase("ADD") && !operator.equalsIgnoreCase("SUB"))
                    writer.println("INVALID OPERATOR");

                //Invalid numbers
                else if(numbers == null)
                    writer.println("INVALID NUMBERS");

                //No errors
                else {
                    double result = operator.equalsIgnoreCase("ADD") ? numbers[0] + numbers[1] : numbers[0] - numbers[1];
                    writer.println("ANS: " + result);
                }

                line = reader.readLine();
            }
        } catch(IOException e){
            e.printStackTrace();
        }

        System.out.println("Client disconnected");
    }

    //Converts string to double
    private static double[] convertToNumbers(String[] input){
        try {
            double[] numbers = {
                    Double.parseDouble(input[1]),
                    Double.parseDouble(input[2])
            };

            return numbers;
        } catch(Exception e){
            return null;
        }
    }
}